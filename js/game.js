var game = (function () {

    var params = {
        host: 'welovehtml.ru',
        port: '9010',
        key: 'peerjs',
        iceServers: [
            {url:'stun:numb.viagenie.ca'},
            {
                url: 'turn:numb.viagenie.ca',
                credential: 'f0r_4dm1n',
                username: 'fromanywhere@yandex.ru'
            }
        ],
        size: {
            width: 4,
            height: 4
        },
        chooseTimeout: 1000,
        updateTimeout: 100
    }

    var otherPeers = {};
    var gameModel = {
        originalModel: [],
        timeStamp: null,
        progressModel: [],
        progressValue: null
    };
    var peer = null;
    var isChooseInProgress = false;
    var blockChoose = false;
    var lastChoosenCoords = {
        x: null,
        y: null
    };

    function getPeersOnline(callback) {
        var request = new XMLHttpRequest();
        request.open('GET', 'http://' + params.host + ':' + params.port + '/' + params.key + '/peers', true);

        function fail() {
            callback(null);
        }

        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                callback(JSON.parse(request.responseText));
            } else {
                fail();
            }
        };

        request.onerror = function() {
            fail();
        };

        request.send();
    }

    function createPeer(name) {
        // создали пира
        var peer = new Peer(name, {
            host: params.host,
            path: '/',
            port: params.port,
            key: params.key,
            config: {
                iceServers: params.iceServers
            }
        });

        // сразу определим его реакцию на то, если к нему кто-то подключится
        peer.on('connection', function (connection) {

            connection.on('open', function () {
                // сначала нужно отдать подключившемуся к пиру модель игры
                connection.send(gameModel);

                // затем добавить подключившегося в список, кому пир будет слать обновления
                otherPeers[connection.peer] = {
                    connection: connection,
                    gameModel: {}
                }

                // затем подписать пир на обновления того, который подключился
                connection.on('data', function (data) {
                    otherPeerUpdateGameModel(connection.peer, data);
                })

                connection.on('close', function (data) {
                    delete otherPeers[connection.peer];
                    renderRating();
                })
            });
        });

        return peer;
    }

    function connectPeer(peer) {

        getPeersOnline(function (otherPeerIdList) {
            // сделать otherPeerIdList без peer
            otherPeerIdList.splice(otherPeerIdList.indexOf(peer.peer), 1);

            // подключиться ко всем по списку
            otherPeerIdList.forEach(function (currentPeerId) {
                // разослать всем пирам в сети, что я подключился
                var connection = peer.connect(currentPeerId);

                connection.on('open', function () {

                    // у себя запомнить всех пиров, которые были онлайн на момент подключения
                    otherPeers[currentPeerId] = {
                        connection: connection,
                        gameModel: {}
                    }

                    // подписать пира на обновления моделей остальных пиров
                    connection.on('data', function (data) {
                        otherPeerUpdateGameModel(currentPeerId, data);
                    })

                    connection.on('close', function (data) {
                        delete otherPeers[currentPeerId];
                        renderRating();
                    })
                });
            });
        })
    }

    // Будет реагировать на изменения модели какого-то подключенного пира
    function otherPeerUpdateGameModel(otherPeerId, data) {
        otherPeers[otherPeerId].gameModel = data;

        // если хеш чужой модели совпадает с нашей, проверить заполненность и обновить рейтинг
        if (otherPeers[otherPeerId].gameModel.originalModel.toString() === gameModel.originalModel.toString()) {
            renderRating();
        }
    }

    function updateGameModel(firstPoint, secontPoint) {
        gameModel.progressModel[firstPoint.y][firstPoint.x] = gameModel.originalModel[firstPoint.y][firstPoint.x];
        gameModel.progressModel[secontPoint.y][secontPoint.x] = gameModel.originalModel[secontPoint.y][secontPoint.x];
        gameModel.progressValue += 2;

        for (var currentOtherPeer in otherPeers) {
            otherPeers[currentOtherPeer].connection.send(gameModel);
        }

        if (gameModel.progressValue === (params.size.width * params.size.height)) {
            document.querySelector('.game-screen').classList.add('invisible');
            document.querySelector('.finish-screen').classList.remove('invisible');
        }

        renderRating();
    }

    function generateModel() {

        // сгенерируем вектор с двойными значениями
        function generateDuplicateSet() {
            var duplicateSet = [];
            var duplicateSetLength = params.size.height * params.size.height / 2;

            for (var i = 0; i < duplicateSetLength; i++) {
                duplicateSet.push(i);
                duplicateSet.push(i);
            }

            return duplicateSet;
        }

        // перемешаем вектор
        function randomiseVector(initialArray) {
            var newArray = [];
            var oldArray = initialArray;
            var arrayLength = oldArray.length;

            for (var i = 0; i < arrayLength; i++) {
                var index = Math.floor(Math.random() * (arrayLength - i));
                newArray.push(oldArray[index]);
                oldArray.splice(index, 1);
            }

            return newArray;
        }

        var model = [];
        var randomVector = randomiseVector(generateDuplicateSet());

        // соберем из вектора матрицу
        for (var h = 0; h < params.size.height; h++) {
            var line = [];

            for (var w = 0; w < params.size.width; w++) {
                line.push(randomVector[h * params.size.width + w]);
            }

            model.push(line);
        }

        return model;
    }

    function generateEmptyModel() {
        var model = [];

        for (var h = 0; h < params.size.height; h++) {
            var line = [];

            for (var w = 0; w < params.size.width; w++) {
                line.push(null);
            }

            model.push(line);
        }

        return model;
    }

    // если никого нет онлайн или принудительно
    function createGameModel(startGame) {
        // содержит оригинальный массив,
        // время создания
        // массив с текущим прогрессом игрока — изначально пустой
        // количество открытых полей

        gameModel = {
            originalModel: generateModel(),
            timeStamp: Date.now(),
            progressModel: generateEmptyModel(),
            progressValue: 0
        }

        startGame();
    }

    // заберем самую свежую модель
    function importGameModel(startGame) {
        var latestModelTimestamp = 0;
        var latestModel = {};

        // перебрать все имеющиеся модели в списке подключенных пиров.
        for (currentOtherPeer in otherPeers) {
            if (otherPeers[currentOtherPeer].gameModel.timeStamp > latestModelTimestamp) {
                latestModelTimestamp = otherPeers[currentOtherPeer].gameModel.timeStamp;
                latestModel = otherPeers[currentOtherPeer].gameModel;
            }
        }

        // если модели есть, выбрать самую свежую, иначе перезапустить таск чуть позже
        if (latestModelTimestamp) {
            gameModel = {
                originalModel: otherPeers[currentOtherPeer].gameModel.originalModel,
                timeStamp: latestModelTimestamp,
                progressModel: generateEmptyModel(),
                progressValue: 0
            }

            // разослать всем событие, что мы определились с моделью
            for (var currentOtherPeer in otherPeers) {
                otherPeers[currentOtherPeer].connection.send(gameModel);
            }

            startGame();
        } else {
            setTimeout(function () {
                importGameModel(startGame);
            }, params.updateTimeout);
        }
    }

    // отрисовать модель на вью
    function renderGameModel() {
        var root = document.getElementById('field');
        var rootContent = '';

        for (var y = 0; y < params.size.height; y++) {
            for (var x = 0; x < params.size.width; x++) {
                rootContent += "<div class='game_elem' data-y='" + y + "' data-x='" + x + "'>" +
                    (function () {
                        return gameModel.progressModel[y][x] !== null
                            ? gameModel.progressModel[y][x]
                            : "";
                    })() +
                    "</div>";
            }
        }

        root.innerHTML = rootContent;
    }

    // отрисовать рейтинг
    function renderRating() {
        var rating = [];
        var root = document.getElementById('rating');
        var rootContent = '';

        // При прочих равных показывать себя выше
        rating.push({
            id: peer.id,
            progress: gameModel.progressValue
        })

        // добавить в рейтинг всех, у кого хеш модели совпадает
        for (currentOtherPeer in otherPeers) {
            if (otherPeers[currentOtherPeer].gameModel.originalModel.toString() === gameModel.originalModel.toString()) {
                rating.push({
                    id: otherPeers[currentOtherPeer].connection.peer,
                    progress: otherPeers[currentOtherPeer].gameModel.progressValue
                })
            }
        }

        // отсортировать по убыванию
        rating.sort(function(a, b) {
            return b.progress - a.progress;
        });

        for (var i = 0; i < rating.length; i++) {
            rootContent += "<div class='game_rating_name " + (function () {
                return rating[i].id === peer.id
                    ? "__current"
                    : ""
            })() + "'>" + rating[i].id + "</div><div class='game_rating_value " + (function() {
                return rating[i].progress === (params.size.width * params.size.height)
                    ? "__winner"
                    : ""
            })() + "'>" + rating[i].progress + "</div>";
        }

        root.innerHTML = rootContent;
    }

    // отрисовать пользователей онлайн
    function renderPeersOnline() {
        getPeersOnline(function (peersOnline) {
            var root = document.getElementById('online-list');
            var rootList = document.getElementById('online-list_lst');
            var rootContent = '';

            root.classList.add('invisible');
            if (peersOnline) {
                root.classList.remove('invisible');

                for (var i = 0; i < peersOnline.length; i++) {
                    rootContent += '<li class="online-item">' + peersOnline[i] + '</li>';
                }

                rootList.innerHTML = rootContent;
            }
        });
    }

    // если пира еще нет, получить список существующих и создать безконфликтно
    // иначе сделать реконнект
    function registerUser(callback) {
        if (!peer) {
            getPeersOnline(function (otherPeerIdList) {
                var nickName = document.getElementById('myname').value;
                document.getElementById('exists').classList.add('invisible');
                if (otherPeerIdList && otherPeerIdList.indexOf(nickName) !== -1) {
                    document.getElementById('exists').classList.remove('invisible');
                    return;
                }

                peer = createPeer(nickName);

                callback(otherPeerIdList);
            });
        } else {
            if (peer.destroyed) {
                peer = null;
            } else {
                peer.destroy();
            }

            setTimeout(function () {
                registerUser(callback);
            }, params.updateTimeout);
        }
    }

    function attachListeners() {
        document.addEventListener('click', function (e) {
            if (blockChoose) {
                return false;
            }

            var targetElement = e.target;

            if (targetElement.classList.contains('game_elem')) {
                var xPosition = targetElement.getAttribute('data-x');
                var yPosition = targetElement.getAttribute('data-y');

                // запрет кликать по одной и той же клетке
                if (xPosition === lastChoosenCoords.x && yPosition === lastChoosenCoords.y) {
                    return false;
                }

                isChooseInProgress = !isChooseInProgress;

                targetElement.innerHTML = gameModel.originalModel[yPosition][xPosition];

                if (!isChooseInProgress && lastChoosenCoords.x !== null) {
                    if (gameModel.originalModel[yPosition][xPosition] === gameModel.originalModel[lastChoosenCoords.y][lastChoosenCoords.x]) {
                        updateGameModel({x: xPosition, y: yPosition}, {x: lastChoosenCoords.x, y: lastChoosenCoords.y})
                    }

                    blockChoose = true;
                    setTimeout(function () {
                        renderGameModel();

                        lastChoosenCoords = {
                            x: null,
                            y: null
                        };

                        blockChoose = false;
                    }, params.chooseTimeout);
                }

                lastChoosenCoords = {
                    x: xPosition,
                    y: yPosition
                }
            }
        })
    }

    function startGame() {
        renderGameModel();
        renderRating();

        document.querySelector('.finish-screen').classList.add('invisible');
        document.querySelector('.welcome-screen').classList.add('invisible');
        document.querySelector('.game-screen').classList.remove('invisible');
    }

    function init() {
        attachListeners();
        renderPeersOnline();
    }

    init();

    return {
        join: function () {

            registerUser(function (otherPeerIdList) {
                if (otherPeerIdList && otherPeerIdList.length) {
                    connectPeer(peer);
                    importGameModel(startGame);
                } else {
                    createGameModel(startGame);
                }
            });

        },
        create: function () {
            registerUser(function (otherPeerIdList) {
                createGameModel(startGame);
            });
        }
    }

})();